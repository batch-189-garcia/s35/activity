const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')

const app = express()
const port = 3001

dotenv.config()

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.tveytx1.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})




let db = mongoose.connection

db.on('error', console.error.bind(console, 'Connection Error'))
db.on('open', () => console.log('Connected to MongoDB!'))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// 1. User Schema
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

// 2. User Model 
const User = mongoose.model('User', userSchema)

// 3.Post request at the /signup route using postman to register a user
let users = []

app.post('/signup', (request, response) => {


    if (request.body.username !== '' && request.body.password !== '') {
        let newUser = new User({
            username: request.body.username,
            password: request.body.password
        })

        newUser.save((error, savedUser) => {
            if (error) {
                return response.send(error)
            }

            return response.send('New user registered')
        })
    }

})


   


// additional/optional functions I included (not part of activity)
// get users
app.get('/users', (request, response) => {
    return User.find({}, (error, result) => {
        if (error) {
            response.send(error)
        }

        response.send(result)
    })
})
// Delete a single User based on given name
app.delete('/delete-any-user', (request, response) => {
    User.deleteOne({ username: request.body.username }, (error, result) => {
        if (error) {
            response.send(error)
        }
        response.send(result)
    })
})

// Delete all users
app.delete('/delete-all', (request, response) => {
    User.deleteMany({ }, (error, result) => {
        if (error) {
            response.send(error)
        }
        response.send(result)
    })
})


app.listen(port, () => console.log(`Server is running at ${port}`))